#!/bin/bash

#converts list of files with parameters suitable in french environment

for i in $@
do
   echo "converting $i to $i.ods"
   @CMAKE_INSTALL_PREFIX@/bin/csv2ods --sep "\t" --num fr $i $i.ods
done


