#!/bin/bash

#converts list of files with parameters suitable in french environment

for i in $@
do
   echo "converting $i (ods) to $i.csv"
   @CMAKE_INSTALL_PREFIX@/bin/ods2csv --sep "\t" --num fr $i $i.csv
done


