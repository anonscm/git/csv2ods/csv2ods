/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libOdsStream.
 * 
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

package fr.inra.pappso.csv2ods;

public enum NumericFormat {
	en, fr
}