/*******************************************************************************
 * Copyright (c) 2011 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libOdsStream.
 * 
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

package fr.inra.pappso.csv2ods;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;

import org.apache.log4j.Logger;

import com.csvreader.CsvWriter;

import fr.inra.pappso.libodsstream.OdsDocReader;
import fr.inra.pappso.libodsstream.reader.OdsCell;
import fr.inra.pappso.libodsstream.reader.OdsSheet;

public class Ods2CsvDocReader extends OdsDocReader implements
		Ods2CsvDocReaderInterface {
	private static final Logger logger = Logger
			.getLogger(Ods2CsvDocReader.class);
	CsvWriter writer = null;
	protected NumericFormat numericFormat;
	private OutputStream sink;
	protected char separator;
	protected Charset charset;

	public Ods2CsvDocReader(InputStream source, OutputStream sink,
			char separator, Charset charset) throws IOException {
		super(source);
		this.sink = sink;
		this.separator = separator;
		this.charset = charset;
		this.writer = null;
	}

	public Ods2CsvDocReader(InputStream source) throws IOException {
		super(source);
	}

	@Override
	public void startSheet(OdsSheet sheet) throws Exception {
		logger.debug("****************** Start sheet *********************");
		logger.debug("****************** " + sheet.getName()
				+ " *********************");
	}

	@Override
	public void endSheet() throws Exception {
		logger.debug("****************** End sheet *********************");
	}

	@Override
	public void startLine() throws Exception {
		logger.debug("****************** Start Line *********************");
	}

	@Override
	public void endLine() throws Exception {
		writer.endRecord();
		logger.debug("****************** End Line *********************");
	}

	@Override
	public void setCell(OdsCell cell) throws Exception {

		try {
			if (cell.isEmpty()) {
				// writer.write("v");
				// logger.debug("****************** Cell is empty *********************");
				writer.write("", true);
			} else {
				if (cell.getStringValue() != null) {
					writer.write(cell.getStringValue(), true);
					// logger.debug("******************  string : "
					// + cell.getStringValue() + " *********************");
				} else if (cell.getDateValue() != null) {
					writer.write("" + cell.getDateValue().getTime(), true);
					// logger.debug("****************** date : "
					// + cell.getDateValue().getTime()
					// + " *********************");
				} else if (cell.getDoubleValue() != null) {
					if (this.numericFormat.equals(NumericFormat.fr)) {
						String num = "" + cell.getDoubleValue();
						writer.write(num.replace(".", ","), true);
					} else {
						writer.write("" + cell.getDoubleValue(), true);
					}
					// logger.debug("****************** float : "
					// + cell.getDoubleValue() + " *********************");
				} else {
					writer.write("", true);
				}
			}
			// logger.debug("***************************************");
		} catch (Exception e) {
			throw new Exception("ERROR in setCell(OdsCell cell) : "
					+ e.getMessage());
		}
	}

	public void setNumericFormat(NumericFormat numericFormat) {
		this.numericFormat = numericFormat;
	}

	/**
	 * callback that report the end of the ODS document. Override it if you need
	 * to know that reading is finished.
	 * 
	 * @throws Exception
	 */
	@Override
	public void endDocument() throws Exception {
		logger.debug("endDocument");
		this.close();

	}

	synchronized public void close() throws IOException {
		if (writer == null) {
			logger.debug("CsvWriter already closed");
		} else {
			logger.debug("closing CsvWriter");
			writer.flush();
			writer.close();
			writer = null;
		}

		if (sink == null) {
			logger.debug("sink already closed");
		} else {
			logger.debug("sink CsvWriter");
			sink.flush();
			sink.close();
			sink = null;
		}

		// logger.debug("closing outputstream");
		// sink.close();
	}

	@Override
	synchronized public void readOdsSource() throws IOException {
		writer = new CsvWriter(sink, separator, charset);
		writer.setUseTextQualifier(false);
		writer.setEscapeMode(CsvWriter.ESCAPE_MODE_BACKSLASH);
		try {
			this.readOds();
		} catch (IOException e) {
			this.close();
			String message = "error in readOdsSource, IOException : "
					+ e.getMessage();
			message += "\n hoping ODS source was closed correctly and forget about it";
			logger.error(message);
			throw e;
		} catch (Exception e) {
			this.close();
			String message = "error in readOdsSource : " + e.getMessage();
			logger.error(message);
			// throw e;
		}

	}
}
