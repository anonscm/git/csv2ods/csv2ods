/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of libOdsStream.
 *
 * libOdsStream is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libOdsStream is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libOdsStream. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors: Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial
 * API and implementation
 ******************************************************************************/
package fr.inra.pappso.csv2ods;

import java.io.File;

import org.apache.log4j.Logger;

import fr.inra.pappso.csv2ods.exceptions.Ods2CsvException;

public class TsvWriterMultiSheet extends Ods2Csv {
	private static final Logger logger = Logger
			.getLogger(TsvWriterMultiSheet.class);

	public TsvWriterMultiSheet(File csvoutputDirectory) throws Ods2CsvException {
		try {
			Ods2Csv ods2csv = new Ods2Csv();
			// ods2csv.setSinkFilename(csvoutputDirectory.getAbsolutePath());
			ods2csv.setSinkDirectoryName(csvoutputDirectory.getAbsolutePath());

			odsTable = ods2csv.getOdsDocPipedOutput();
		} catch (Ods2CsvException e) {
			String message = "Error creating TsvWriterMultiSheet object :\n"
					+ e.getMessage();
			logger.error(message);
			throw (e);
		}

		catch (Exception e) {
			String message = "Error creating TsvWriterMultiSheet object :\n"
					+ e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message, e);
		}

	}

	@Override
	public void writeSheet(String arg0) throws Exception {
		if (this.odsTable == null) {
			String message = "this.odsTable == null";
			logger.error(message);
			throw new Ods2CsvException(message);

		}
		this.odsTable.writeSheet(arg0+".tsv");
	}

}