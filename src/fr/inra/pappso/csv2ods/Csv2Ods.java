package fr.inra.pappso.csv2ods;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;

import com.csvreader.CsvReader;

import fr.inra.pappso.libodsstream.OdsDocWriter;

/*******************************************************************************
 * Copyright (c) 2012 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libOdsStream.
 * 
 * libOdsStream is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * libOdsStream is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * libOdsStream. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors: Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial
 * API and implementation
 ******************************************************************************/

public class Csv2Ods {
	private static final Logger logger = Logger.getLogger(Csv2Ods.class);

	InputStream source = null;
	OutputStream sink = null;
	private char separator = '\t';

	private String numericFormat = "en";

	public void setSourceInputStream(InputStream fileInputStream) {
		source = fileInputStream;
	}

	public void setSourceFilename(String inputCsvFilename) {
		try {
			source = new FileInputStream(inputCsvFilename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setSinkFilename(String outputOdsFilename) {
		try {
			sink = new FileOutputStream(outputOdsFilename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run() {
		OdsDocWriter odsWriter = null;
		try {
			odsWriter = new OdsDocWriter(this.sink);
			odsWriter.writeSheet("csv2ods");

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (XMLStreamException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DatatypeConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Get the object of DataInputStream
		if (source == null) {
			logger.error("source is null");
		}
		DataInputStream in = new DataInputStream(source);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));

		CsvReader reader = new CsvReader(br);
		reader.setDelimiter(this.separator);
		try {
			while (reader.readRecord()) {
				odsWriter.writeLine();
				String[] values = reader.getValues();
				for (int i = 0; i < values.length; i++) {
					String currentValue = values[i];
					if (currentValue == null) {
						odsWriter.writeEmptyCell();
					}
					if (writeNumber(odsWriter, currentValue)) {

					} else if (writeDate(odsWriter, currentValue)) {

					} else if (writeString(odsWriter, currentValue)) {

					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} finally {
			try {
				odsWriter.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (XMLStreamException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			try {
				this.sink.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		this.sink = null;
	}

	private boolean writeString(OdsDocWriter odsWriter, String currentValue) {
		try {
			odsWriter.writeCell(currentValue);
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return true;
	}

	private boolean writeDate(OdsDocWriter odsWriter, String currentValue) {
		// TODO Auto-generated method stub
		return false;
	}

	private boolean writeNumber(OdsDocWriter odsWriter, String currentValue) {
		try {
			double f;
			if (this.numericFormat.equals("fr")) {
				f = Double.valueOf(currentValue.trim().replace(",", "."))
						.doubleValue();
			} else {
				f = Double.valueOf(currentValue.trim()).doubleValue();
			}
			odsWriter.writeCell(f);
			return true;
			// System.out.println("double f = " + f);
		} catch (NumberFormatException nfe) {
			// System.out.println("NumberFormatException: " + nfe.getMessage());
			return false;
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return false;
	}

	public void setSeparator(char optionValue) {
		this.separator = optionValue;
	}

	public void setSinkOutputStream(OutputStream out) {
		sink = out;
	}

	public void setNumericFormat(String optionValue) {
		if (optionValue == null) {
			return;
		}
		if (optionValue.equals("fr") || optionValue.equals("en")) {
			this.numericFormat = optionValue;
		} else {
			this.numericFormat = "en";
		}
	}
}
