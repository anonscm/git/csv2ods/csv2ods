/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of libOdsStream.
 *
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

package fr.inra.pappso.csv2ods;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import org.apache.log4j.Logger;

import com.csvreader.CsvWriter;

import fr.inra.pappso.libodsstream.reader.OdsSheet;

public class Ods2MultiCsvDocReader extends Ods2CsvDocReader implements
		Ods2CsvDocReaderInterface {
	private static final Logger logger = Logger
			.getLogger(Ods2MultiCsvDocReader.class);
	private File directory;

	public Ods2MultiCsvDocReader(InputStream in, File directory,
			char separator, Charset charset) throws Exception {
		super(in);
		this.writer = null;
		this.directory = directory;
		this.separator = separator;
		this.charset = charset;
		if (directory.exists()) {
			String message = directory.getAbsolutePath() + " already exists";
			logger.error(message);
			throw new Exception(message);
		} else {
			try {
				directory.mkdir();
			} catch (Exception e) {
				String message = "unable to create directory : "
						+ directory.getAbsolutePath();
				logger.error(message);
				throw new Exception(message);
			}
		}
		if (!directory.isDirectory()) {
			String message = directory.getAbsolutePath()
					+ " is not a directory";
			logger.error(message);
			throw new Exception(message);
		}
		if (!directory.canWrite()) {
			String message = directory.getAbsolutePath() + " is not writeable";
			logger.error(message);
			throw new Exception(message);
		}
	}

	@Override
	public void startSheet(OdsSheet sheet) throws Exception {
		logger.debug("****************** Start sheet *********************");
		logger.debug("****************** " + sheet.getName()
				+ " *********************");
		File sinkFile = new File(directory.getAbsolutePath().concat(
				File.separator + sheet.getName()));
		writer = new CsvWriter(sinkFile.getAbsolutePath(), separator, charset);

	}

	@Override
	public void endSheet() throws Exception {
		logger.debug("****************** End sheet *********************");
		if (writer != null) {
			writer.flush();
			writer.close();
		}
		writer = null;
	}

	@Override
	synchronized public void readOdsSource() throws IOException {
		try {
			this.readOds();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
			writer = null;
		}
	}
}
