package fr.inra.pappso.csv2ods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.URI;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.Duration;
import javax.xml.stream.XMLStreamException;

import org.apache.log4j.Logger;

import fr.inra.pappso.csv2ods.exceptions.Ods2CsvException;
import fr.inra.pappso.libodsstream.OdsDocWriter;
import fr.inra.pappso.libodsstream.OdsDocWriterInterface;

/*******************************************************************************
 * Copyright (c) 2012 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of libOdsStream.
 *
 * libOdsStream is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * libOdsStream is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * libOdsStream. If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors: Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial
 * API and implementation
 ******************************************************************************/

public class Ods2Csv implements Runnable, OdsDocWriterInterface {
	private static final Logger logger = Logger.getLogger(Ods2Csv.class);

	InputStream source;
	OutputStream sink;
	private char separator = '\t';
	private Charset charset = Charset.availableCharsets().get("UTF-8");

	private NumericFormat numericFormat = NumericFormat.en;

	private File directory = null;

	private boolean isRunning = false;

	private Thread privThread;

	protected OdsDocWriter odsTable;

	public void setSourceInputStream(InputStream fileInputStream)
			throws Exception {
		if (this.isRunning) {
			String message = "unable to setSinkFilename : Ods2Csv is already running";
			logger.error(message);
			throw new Exception(message);
		}
		source = fileInputStream;
	}

	public void setSourceFilename(String inputOdsFilename) throws Exception {
		if (this.isRunning) {
			String message = "unable to setSinkFilename : Ods2Csv is already running";
			logger.error(message);
			throw new Exception(message);
		}
		logger.debug("setSourceFilename " + inputOdsFilename);
		try {
			source = new FileInputStream(inputOdsFilename);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setSinkDirectoryName(String outputCsvDirectoryname)
			throws Exception {
		if (this.isRunning) {
			String message = "unable to setSinkFilename : Ods2Csv is already running";
			logger.error(message);
			throw new Exception(message);
		}
		logger.debug("setSinkDirectory for multiCsv " + outputCsvDirectoryname);
		this.directory = new File(outputCsvDirectoryname);
	}

	public void setSinkFilename(String outputCsvFilename)
			throws Ods2CsvException {

		try {
			if (this.isRunning) {
				String message = "unable to setSinkFilename : Ods2Csv is already running";
				logger.error(message);
				throw new Ods2CsvException(message);
			}

			logger.debug("setSinkFilename " + outputCsvFilename);
			try {
				sink = new FileOutputStream(outputCsvFilename);
			} catch (FileNotFoundException e) {
				String message = "Error creating output CSV file :\n"
						+ e.getMessage();
				logger.error(message);
				throw new Ods2CsvException(message);
			}
		} catch (Ods2CsvException e) {
			throw (e);
		}

		catch (Exception e) {
			String message = "Error in setSinkFilename :\n" + e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message, e);
		}
	}

	public void run() {
		this.isRunning = true;
		Ods2CsvDocReaderInterface odsReader = null;
		try {

			if (directory != null) {
				odsReader = new Ods2MultiCsvDocReader(this.source, directory,
						separator, charset);
			} else {
				odsReader = new Ods2CsvDocReader(this.source, sink, separator,
						charset);
			}
			odsReader.setNumericFormat(this.numericFormat);
			odsReader.readOdsSource();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			odsReader.close();
		} catch (IOException e) {
			String message = "IOException closing the doc reader "
					+ e.getMessage();
			logger.error(message);
			// throw new Exception(message);
		}
		this.sink = null;
	}

	public void setSeparator(char optionValue) throws Exception {
		if (this.isRunning) {
			String message = "unable to setSinkFilename : Ods2Csv is already running";
			logger.error(message);
			throw new Exception(message);
		}
		this.separator = optionValue;
	}

	public void setSinkOutputStream(OutputStream out) throws Exception {
		if (this.isRunning) {
			String message = "unable to setSinkFilename : Ods2Csv is already running";
			logger.error(message);
			throw new Exception(message);
		}
		sink = out;
	}

	public void setNumericFormat(NumericFormat numericFormat) throws Exception {
		if (this.isRunning) {
			String message = "unable to setSinkFilename : Ods2Csv is already running";
			logger.error(message);
			throw new Exception(message);
		}
		if (numericFormat == null) {
			return;
		}
		this.numericFormat = numericFormat;
	}

	public OdsDocWriter getOdsDocPipedOutput() throws Ods2CsvException {
		// OdsDocWriter odsTable = null;
		try {

			PipedOutputStream pos = new PipedOutputStream();
			PipedInputStream pis = new PipedInputStream(pos);

			odsTable = new OdsDocWriter(pos);

			this.setSourceInputStream(pis);
			// ods2csv.setSeparator('\t');
			privThread = new Thread(this);
			privThread.start();
			return odsTable;
			// ods2csv.run();
		} catch (IOException e) {
			String message = "IOException " + e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message);
		} catch (XMLStreamException e) {
			String message = "XMLStreamException " + e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message);
		} catch (DatatypeConfigurationException e) {
			String message = "DatatypeConfigurationException " + e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message);
		} catch (Exception e) {
			String message = "Exception in getOdsDocPipedOutput "
					+ e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message);
		}
	}

	public void close() throws Ods2CsvException {
		try {
			try {
				this.odsTable.close();
			} catch (Exception e1) {
				String message = "ERROR in Ods2Csv close function :\n"
						+ e1.getMessage();
				logger.error(message);
				throw new Ods2CsvException(message, e1);
			}

			if (privThread != null) {
				try {
					privThread.join();
				} catch (InterruptedException e) {
					String message = "ERROR in Ods2Csv close function waiting for thread to finish :\n"
							+ e.getMessage();
					logger.error(message);
					throw new Ods2CsvException(message, e);
				}
			}

		} catch (Ods2CsvException e) {
			throw e;
		} catch (Exception e) {
			String message = "ERROR closing Ods2Csv stream:\n" + e.getMessage();
			logger.error(message);
			throw new Ods2CsvException(message, e);
		}
	}

	@Override
	public void writeCell(String arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(int arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(float arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(double arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(boolean arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(Date arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(Timestamp arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(GregorianCalendar arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(Duration arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(Object arg0) throws Exception {
		this.odsTable.writeCell(arg0);
	}

	@Override
	public void writeCell(URI arg0, String arg1) throws Exception {
		this.odsTable.writeCell(arg0, arg1);
	}

	@Override
	public void writeEmptyCell() throws Exception {
		this.odsTable.writeEmptyCell();
	}

	@Override
	public void writeLine() throws Exception {
		this.odsTable.writeLine();
	}

	@Override
	public void writeSheet(String arg0) throws Exception {
		this.odsTable.writeSheet(arg0);
	}

}
