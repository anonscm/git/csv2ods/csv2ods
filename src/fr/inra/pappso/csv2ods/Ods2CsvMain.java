package fr.inra.pappso.csv2ods;

/*******************************************************************************
 * Copyright (c) 2012 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libOdsStream.
 * 
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
//import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * convert OpenDocument Spreadsheet to CSV files files
 * 
 * @param first
 *            argument : the ODS input filename
 * @param second
 *            argument : the CSV input filename
 * 
 * @author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 * 
 */
public class Ods2CsvMain {
	private static final Logger logger = Logger.getLogger(Ods2CsvMain.class);

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws IOException {
		PropertyConfigurator.configure(Ods2CsvMain.class
				.getResource("/conf/log4j.properties"));
		CommandLineParser parser = new GnuParser();

		// create the Options
		Options options = new Options();
		options.addOption("m", "multi", false,
				"write ODS multiple sheets to several csv files into one directory");
		options.addOption("h", "help", false, "help");
		options.addOption("i", "stdin", false, "get stdin as ods input");
		Option option = OptionBuilder.withLongOpt("sep")
				.withDescription("define column separator").hasArg()
				.withArgName("CHAR").withType(Character.class).create();
		options.addOption(option);
		options.addOption(OptionBuilder.withLongOpt("num")
				.withDescription("numeric format (fr or en)").hasArg()
				.withArgName("NUM").withType(String.class).create());
		/*
		 * options.addOption("A", "almost-all", false,
		 * "do not list implied . and .."); options.addOption("b", "escape",
		 * false, "print octal escapes for nongraphic " + "characters");
		 * options.addOption("B", "ignore-backups", false,
		 * "do not list implied entried " + "ending with ~");
		 * options.addOption("c", false,
		 * "with -lt: sort by, and show, ctime (time of last " +
		 * "modification of file status information) with " +
		 * "-l:show ctime and sort by name otherwise: sort " + "by ctime");
		 * options.addOption("C", false, "list entries by columns");
		 */
		try {
			logger.debug("launch");
			String inputOdsFilename = null;
			String outputCsvFilename = null;
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("ods2csv <ods file> <csv file>", options);
				return;
			}
			args = cmd.getArgs();
			if (args.length > 0) {
				inputOdsFilename = args[0];
			}
			if (args.length > 1) {
				outputCsvFilename = args[1];
			}

			Ods2Csv ods2csv = new Ods2Csv();
			if (cmd.hasOption("sep")) {
				String charStr = new String(cmd.getOptionValue("sep"));
				charStr = charStr.replace("\\f", "\f");
				charStr = charStr.replace("\\b", "\b");
				charStr = charStr.replace("\\n", "\n");
				charStr = charStr.replace("\\r", "\r");
				charStr = charStr.replace("\\t", "\t");
				logger.debug("cmd.getOptionValue(sep) " + charStr);
				logger.debug("cmd.getOptionValue(sep) " + charStr.charAt(0));
				ods2csv.setSeparator(charStr.charAt(0));
			}
			if (cmd.hasOption("num")) {
				if (cmd.getOptionValue("num").equals("fr")) {
					ods2csv.setNumericFormat(NumericFormat.fr);
				}
			}
			if (cmd.hasOption("i")) {
				ods2csv.setSourceInputStream(System.in);
				outputCsvFilename = inputOdsFilename;
			} else {
				if (inputOdsFilename == null) {
					System.out.println("input ods filename is null");
					System.out.println("use --help");
					return;
				} else {
					ods2csv.setSourceFilename(inputOdsFilename);
				}
			}
			if (outputCsvFilename == null) {
				if (cmd.hasOption("m")) {
					System.out
							.println("-m option must be used with a valid output path");
					return;
				}
				ods2csv.setSinkOutputStream(System.out);
			} else {
				if (cmd.hasOption("m")) {
					ods2csv.setSinkDirectoryName(outputCsvFilename);
				} else {
					ods2csv.setSinkFilename(outputCsvFilename);
				}
			}
			// csv2ods.setSourceInputStream(new
			// FileInputStream(inputCsvFilename));
			ods2csv.run();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
