package fr.inra.pappso.csv2ods;

/*******************************************************************************
 * Copyright (c) 2012 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libOdsStream.
 * 
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.GnuParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
//import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * convert CSV files to OpenDocument Spreadsheet files
 * 
 * @param first
 *            argument : the CSV input filename
 * @param second
 *            argument : the ODS input filename
 * 
 * @author Olivier Langella <Olivier.Langella@moulon.inra.fr>
 * 
 */
public class Csv2OdsMain {
	private static final Logger logger = Logger.getLogger(Csv2OdsMain.class);

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws IOException {
		PropertyConfigurator.configure(Csv2OdsMain.class
				.getResource("/conf/log4j.properties"));
		CommandLineParser parser = new GnuParser();

		// create the Options
		Options options = new Options();
		options.addOption("h", "help", false, "help");
		options.addOption("i", "stdin", false, "get stdin as csv input");
		Option option = OptionBuilder.withLongOpt("sep")
				.withDescription("define column separator").hasArg()
				.withArgName("CHAR").withType(String.class).create();
		options.addOption(option);
		options.addOption(OptionBuilder.withLongOpt("num")
				.withDescription("numeric format (fr or en)").hasArg()
				.withArgName("NUM").withType(String.class).create());
		/*
		 * options.addOption("A", "almost-all", false,
		 * "do not list implied . and .."); options.addOption("b", "escape",
		 * false, "print octal escapes for nongraphic " + "characters");
		 * options.addOption("B", "ignore-backups", false,
		 * "do not list implied entried " + "ending with ~");
		 * options.addOption("c", false,
		 * "with -lt: sort by, and show, ctime (time of last " +
		 * "modification of file status information) with " +
		 * "-l:show ctime and sort by name otherwise: sort " + "by ctime");
		 * options.addOption("C", false, "list entries by columns");
		 */
		try {
			logger.debug("launch");
			String inputCsvFilename = null;
			String outputOdsFilename = null;
			CommandLine cmd = parser.parse(options, args);
			if (cmd.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("csv2ods <csv file> <ods file>", options);
				return;
			}
			args = cmd.getArgs();
			if (args.length > 0) {
				inputCsvFilename = args[0];
			}
			if (args.length > 1) {
				outputOdsFilename = args[1];
			}

			Csv2Ods csv2ods = new Csv2Ods();
			if (cmd.hasOption("sep")) {
				String charStr = new String(cmd.getOptionValue("sep"));
				charStr = charStr.replace("\\f", "\f");
				charStr = charStr.replace("\\b", "\b");
				charStr = charStr.replace("\\n", "\n");
				charStr = charStr.replace("\\r", "\r");
				charStr = charStr.replace("\\t", "\t");
				logger.debug("cmd.getOptionValue(sep) "
						+ charStr);
				logger.debug("cmd.getOptionValue(sep) "
						+ charStr.charAt(0));
				csv2ods.setSeparator(charStr.charAt(0));
			}
			if (cmd.hasOption("num")) {
				csv2ods.setNumericFormat(cmd.getOptionValue("num"));
			}
			if (cmd.hasOption("i")) {
				csv2ods.setSourceInputStream(System.in);
				outputOdsFilename = inputCsvFilename;
			} else {
				if (inputCsvFilename == null) {
					System.out.println("input csv filename is null");
					System.out.println("use --help");
					return;
				} else {
					csv2ods.setSourceFilename(inputCsvFilename);
				}
			}
			if (outputOdsFilename == null) {
				csv2ods.setSinkOutputStream(System.out);
			} else {
				csv2ods.setSinkFilename(outputOdsFilename);
			}
			// csv2ods.setSourceInputStream(new
			// FileInputStream(inputCsvFilename));
			csv2ods.run();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
