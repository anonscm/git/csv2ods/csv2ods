package fr.inra.pappso.csv2ods;

import java.io.IOException;

public interface Ods2CsvDocReaderInterface {

	void setNumericFormat(NumericFormat numericFormat);

	void readOdsSource() throws IOException;

	void close() throws IOException;
}
