package tests;

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of libOdsStream.
 *
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

//import org.apache.commons.cli.PosixParser;
import java.io.File;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import fr.inra.pappso.csv2ods.TsvWriterMultiSheet;

public class TestOds2MultiCsvMain {
	private static final Logger logger = Logger
			.getLogger(TestOds2MultiCsvMain.class);

	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure(TestOds2MultiCsvMain.class
				.getResource("/conf/log4j.properties"));

		TsvWriterMultiSheet ods2csv;
		ods2csv = new TsvWriterMultiSheet(new File("/tmp/choses"));
		ods2csv.writeSheet("choses");
		ods2csv.writeCell("coucou y\t iutyui");
		ods2csv.writeLine();
		// doc.writeCell(" ");
		ods2csv.writeLine();
		// doc.writeCell(" ");
		ods2csv.writeLine();
		ods2csv.writeCell("coucou2");
		ods2csv.writeCell(new Double(0.5));
		ods2csv.writeSheet("choses2");
		ods2csv.writeCell("coucou3");
		ods2csv.close();

	}
}
