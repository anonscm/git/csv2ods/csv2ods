package fr.inra.pappso.csv2ods;

/*******************************************************************************
 * Copyright (c) 2013 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * 
 * This file is part of libOdsStream.
 * 
 *     libOdsStream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     libOdsStream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with libOdsStream.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and implementation
 ******************************************************************************/

//import org.apache.commons.cli.PosixParser;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import fr.inra.pappso.libodsstream.OdsDocWriter;

public class TestOds2CsvMain {
	private static final Logger logger = Logger
			.getLogger(TestOds2CsvMain.class);

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure(TestOds2CsvMain.class
				.getResource("/conf/log4j.properties"));

		Ods2Csv ods2csv = new Ods2Csv();
		ods2csv.setNumericFormat(NumericFormat.fr);
		ods2csv.setSinkOutputStream(System.out);
		OdsDocWriter doc = ods2csv.getOdsDocPipedOutput();
		doc.writeCell("coucou y\t iutyui");
		doc.writeLine();
		// doc.writeCell(" ");
		doc.writeLine();
		// doc.writeCell(" ");
		doc.writeLine();
		doc.writeCell("coucou2");
		doc.writeCell(new Double(0.5));
		doc.close();

	}
}
